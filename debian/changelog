gpxviewer (1.2.0-2) unstable; urgency=medium

  * Fix syntax error

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 17 Sep 2024 13:53:36 +0200

gpxviewer (1.2.0-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * New upstream version 1.2.0
    - Add support for more external editors (Closes: #1078532)
  * Drop patches (merged upstream)
  * Bump policy version (no changes)

  [ Wookey ]
  * Fix clean after build (Closes: #1078533, #1044311)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 17 Sep 2024 11:38:41 +0200

gpxviewer (1.1.0-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Contact.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Repository-Browse.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 24 Nov 2022 16:44:19 +0000

gpxviewer (1.1.0-4) unstable; urgency=medium

  * Update homepage (Closes: #981596)
  * Add upstream patch for date/time label
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 11 Sep 2021 20:50:19 +0200

gpxviewer (1.1.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Trim trailing whitespace.

  [ Jochen Sprickerhof ]
  * Add patch for osm-gps-map 1.2.0
  * bump policy version (no changes)
  * drop gitlab-ci

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 02 Mar 2021 12:26:47 +0100

gpxviewer (1.1.0-2) unstable; urgency=medium

  * Added missing dependency librsvg2-common
  * bump debhelper version

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 16 May 2020 23:56:44 +0200

gpxviewer (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 25 Mar 2020 22:33:34 +0100

gpxviewer (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1
  * Drop patch (merged upstream)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 11 Oct 2019 22:52:01 +0200

gpxviewer (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0
  * rewrite d/copyright
  * Switch maintainer to PAPT and add myself as uploader
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * add Salsa CI
  * Add R³
  * Drop python-gtk2 (Closes: #941875)
  * Switch d/s/format to 3.0
  * Switch to Python 3 (Closes: #936646, #939105)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 11 Oct 2019 10:26:00 +0200

gpxviewer (0.5.2-2) unstable; urgency=medium

  * Team upload.
  [ SVN-Git Migration ]
  * Update Vcs fields for git migration
  * git-buildpackage config for DEP14 branches

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces
  * Remove debian/pycompat, it's not used by any modern Python helper

  [ Jochen Sprickerhof ]
  * Add missing dependencies.
    Thanks to Hans Joachim Desserud (Closes: #853092)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 18 Apr 2019 19:52:25 +0200

gpxviewer (0.5.2-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Andrew Gee ]
  * New upstream release
  * Removed python-osmgpsmap from Depends, now using gi introspection
    Closes: #739117
  * Removed python-support, now using dh_python2
    Closes: #786027
  * Removed python-gnome2, now using gi libraries
    Closes: #790592

 -- Andrew Gee <andrew@andrewgee.org>  Sat, 18 Jul 2015 22:15:37 +0100

gpxviewer (0.4.3-1) unstable; urgency=low

  * New upstream release
      * Fixes rounding error. Closes: #664494
  * Fixes double build error. Closes: #671212

 -- Andrew Gee <andrew@andrewgee.org>  Sat, 12 May 2012 13:22:22 +0100

gpxviewer (0.4.1-2) UNRELEASED; urgency=low

  * Add Vcs-* fields.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 10 Sep 2011 00:23:08 +0200

gpxviewer (0.4.1-1) unstable; urgency=low

  * New upstream release
  * Converted to use dh sequencer, from cdbs
  * Python module now installed to private /usr/share/gpxviewer

 -- Andrew Gee <andrew@andrewgee.org>  Sat, 15 Jan 2011 13:03:30 +0000

gpxviewer (0.4.0-1) unstable; urgency=low

  * New upstream release
  * Removed no longer required dependency python-glade2

 -- Andrew Gee <andrew@andrewgee.org>  Fri, 31 Dec 2010 00:30:16 +0000

gpxviewer (0.1.5-1) unstable; urgency=low

  * New upstream release
  * Added missing dependency librsvg2-common

 -- Andrew Gee <andrew@andrewgee.org>  Thu, 06 May 2010 20:06:45 +0100

gpxviewer (0.1.4-1) unstable; urgency=low

  * New upstream release

 -- Andrew Gee <andrew@andrewgee.org>  Fri, 30 Apr 2010 19:01:55 +0100

gpxviewer (0.1.3-1) unstable; urgency=low

  * New upstream release

 -- Andrew Gee <andrew@andrewgee.org>  Sun, 28 Mar 2010 22:35:11 +0100

gpxviewer (0.1.2-1) unstable; urgency=low

  * Initial release (Closes: #516609)

 -- Andrew Gee <andrew@andrewgee.org>  Mon, 22 Mar 2010 19:17:46 +0100
